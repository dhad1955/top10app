//
//  SearchTop10Controller.swift
//  Top10New
//
//  Created by daniel hadland on 01/08/2016.
//  Copyright © 2016 daniel hadland. All rights reserved.
//

import UIKit
import SwiftyJSON

class SearchTop10Controller: UIViewController, UISearchBarDelegate,  UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var searcher: UISearchBar!
    @IBOutlet weak var table: UITableView!

    @IBOutlet weak var tableView: UITableViewCell!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var searchController: UISearchController?;
    
    var searchResult: [JSON]? = [];
    
    var searchIndex = 0;
    
    override func viewDidLoad() {
        
        self.modalTransitionStyle = .CrossDissolve;
        super.viewDidLoad()
        
        self.table.delegate = self;
        self.table.dataSource = self;
        searcher.becomeFirstResponder();
        
        searcher.delegate = self;
        self.table.hidden = true;
 
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchResult!.count;
    }
    
    override func viewWillAppear(animated: Bool) {
        searchController?.active = true;
    }
    
    override func viewDidAppear(animated: Bool) {        
     
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell =  self.table.dequeueReusableCellWithIdentifier("searchresult") as! SearchCell;
        
        let data = self.searchResult![indexPath.row];
        
        cell.titleLabel.text = data.dictionaryValue["name"]!.stringValue;
        
        cell.infoLabel.text = data.dictionaryValue["ordinal"]!.stringValue;
        
        let thumb = data.dictionaryValue["thumbnail"]!.stringValue;
        
        
        let url =  NSURL(string: "http://top10.asccio.net/images/\(thumb)");
        
        cell.imageOutlet.kf_setImageWithURL(url!);
        return cell;
        
    
    }
    
    
    func didPresentSearchController(_searchController: UISearchController) {
        self.searchController?.searchBar.becomeFirstResponder()
        
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
 
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        let searchString = searchText;
        
        if(searchString.length < 2) {
            self.searchResult = [];
            self.table.reloadData();
            self.searchIndex += 1;
            return;
        }
        
        if(App.searchTable == nil) {
            return;
        }
        
        self.searchIndex += 1;
        let key = self.searchIndex;
        let qualityOfServiceClass = QOS_CLASS_BACKGROUND
        let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
        dispatch_async(backgroundQueue, {
            self.updateSearchResults(searchString, searchKey: key);
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.table.reloadData()
                self.table.hidden = self.searchResult?.count == 0;
            })
        });
        
    }
    
    
    func updateSearchResults(searchString: String, searchKey: Int) {
        
        var total = 0;
        
        let res = App.searchTable?.arrayValue.filter { result in
            
            if(total > 15 || searchKey != self.searchIndex) {
                return false;
            }
            
            let isFound = result.dictionaryValue["name"]!.stringValue.lowercaseString.containsString(searchString.lowercaseString);
            
            if(isFound) {
                total += 1;
            }
            return isFound;
        }
        if(searchKey == self.searchIndex) {
            self.searchResult = res;
        }
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
