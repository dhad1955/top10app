//
//  Top10ListCell.swift
//  Top10New
//
//  Created by daniel hadland on 04/07/2016.
//  Copyright © 2016 daniel hadland. All rights reserved.
//

import SwiftyJSON
import UIKit

class Top10ListCell: UITableViewCell {

    @IBOutlet weak var listTitle: UILabel!
    @IBOutlet weak var updatedLabel: UILabel!
    
    @IBOutlet weak var listThumbnail: UIImageView!
    
    @IBOutlet weak var titleLabel: UIView!
    
    var listData: JSON?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
