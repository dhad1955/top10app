//
//  ViewRankController.swift
//  Top10New
//
//  Created by daniel hadland on 17/07/2016.
//  Copyright © 2016 daniel hadland. All rights reserved.
//

import SwiftyJSON
import UIKit

class ViewRankController: UIViewController {

    @IBOutlet weak var numberLabel: UILabel!
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var extraInfoLabel: UILabel!
    @IBOutlet weak var listTitleLabel: UILabel!
    public var data: [String: JSON]?;
    
    @IBOutlet weak var rankValueLabel: UILabel!
    
    @IBOutlet weak var infoTextLabel: UILabel!
    @IBOutlet weak var vHeightConstraint: NSLayoutConstraint!
    var pageIndex: Int = 0;
    
    @IBOutlet weak var imgHeight: NSLayoutConstraint!
    @IBOutlet weak var imgWidth: NSLayoutConstraint!
    @IBOutlet weak var imageOutlet: UIImageView!
  
    
     var listTitle:  String?;
    
     var unitValue:  String?;
    
    @IBOutlet weak var nameLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad();
        
        let image = UIImageView(image: UIImage(named: "top10logo"));
        
        image.frame = CGRect(x: 0, y: 0, width: 88, height: 44);
        image.contentMode = .ScaleAspectFit
        self.navigationItem.titleView = image;
        
        
        
        // START SMALL IPHONE
        
        // 165 CG FLOAT (IPHONE 6 AND UP)
        
        
        vHeightConstraint.constant = CGFloat(123);
        
//        self.numberLabel.hidden = true;
//        self.extraInfoLabel.hidden = true;
        // END SMALL IPHONE
        
        
        let extraData = self.data!["data"]!;
        
        if let thumbnail = extraData.dictionaryValue["thumbnail"] {
        
            let url = NSURL(string: "http://top10.asccio.net/images/\(thumbnail.stringValue)");
            print(thumbnail);
            
            self.imageOutlet.kf_setImageWithURL(url!, placeholderImage: nil)  ;
        }
    
        
        let pagenumber = String(self.pageIndex + 1);
        
        self.pageControl.currentPage = self.pageIndex;

        
//        self.numberLabel.text = pagenumber+".";
        self.nameLabel.text = self.data!["name"]?.stringValue;
        self.extraInfoLabel.text = self.data!["extra"]?.stringValue;
        
        let val = self.data!["value"]?.stringValue;
        
        let formatter = NSNumberFormatter();
        formatter.numberStyle = NSNumberFormatterStyle.DecimalStyle;
        
        let doubleFormatter = NSNumberFormatter();
        doubleFormatter.maximumFractionDigits = 2;
        let intVal = Int(val!);
        
        let unit = self.unitValue;
        
        self.listTitleLabel.text = self.listTitle;
        
        let doubleVal = Double(val!);
        let formatted: String = intVal != nil ? formatter.stringFromNumber(NSNumber(integer: intVal!))! : doubleFormatter.stringFromNumber(NSNumber(double: doubleVal!))!;
        
        
        self.rankValueLabel.text = formatted+" \(unit!)";
        self.nameLabel.sizeToFit();
        
        
    }
        
        
    
    @IBOutlet weak var bHeight: NSLayoutConstraint!
 
    @IBOutlet weak var imageWidthConstraint: NSLayoutConstraint!

    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

 

}
