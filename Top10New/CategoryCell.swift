//
//  CategoryCell.swift
//  Top10New
//
//  Created by daniel hadland on 03/07/2016.
//  Copyright © 2016 daniel hadland. All rights reserved.
//

import UIKit
import SwiftyJSON
import Spring

class CategoryCell: UICollectionViewCell {
    
    var scale = 2;
 
    @IBOutlet weak var progressConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
 
    @IBOutlet weak var leftLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightLayoutConstraint: NSLayoutConstraint!
    

    @IBOutlet weak var rightLabelConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var leftLabelConstraint: NSLayoutConstraint!
    @IBOutlet weak var catName: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
    
    @IBOutlet weak var topLayoutConstraint: NSLayoutConstraint!
    
    private var scaleSize : CGFloat = CGFloat(0);
    func scale(itemSize: CGFloat) {
        
        self.scaleSize = itemSize;
        leftLayoutConstraint.constant = itemSize / 4;
        rightLayoutConstraint.constant = itemSize / 4;
        
        topLayoutConstraint.constant = itemSize / 4;
        
        if(itemSize < 100) {
            self.catName.font = self.catName.font.fontWithSize(10);
        }
        print("item sizzzeee \(itemSize)");
    }
    
    
    func getPercentRead() -> Float {
        
        let total = 20;
        
        let category_id = self.category!["id"];
        
        let cat_key = "cat_count_\(category_id)";
        let defaults = NSUserDefaults.standardUserDefaults();
        let sub_total = defaults.integerForKey(cat_key);
        var percentage = Float(Float(sub_total) / Float(total));
        return percentage;
    }
    
 
    var category: [String: JSON]?
    
}
