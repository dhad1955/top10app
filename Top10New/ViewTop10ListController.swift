//
//  ViewTop10ListController.swift
//  Top10New
//
//  Created by daniel hadland on 04/07/2016.
//  Copyright © 2016 daniel hadland. All rights reserved.
//

import UIKit
import SwiftyJSON

class ViewTop10ListController: UITableViewController {
    
    
    var backgroundColor: UIColor?;
    var listData : JSON?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let color = UIColor(hue: 0.1167, saturation: 0.04, brightness: 0.18, alpha: 1.0);
        let toColor = UIColor(hue: 0.3694, saturation: 0.04, brightness: 0.32, alpha: 1.0);
        
        self.setTableViewBackgroundGradient(self, color, toColor);
    }
    
    func setTableViewBackgroundGradient(sender: UITableViewController, _ topColor:UIColor, _ bottomColor:UIColor) {

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listData!["items"].arrayValue.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)

        // Configure the cell...
        cell.accessoryType = UITableViewCellAccessoryType.DetailButton

        cell.backgroundColor = UIColor.clearColor()
        
        let transform = cell as! RankCell
        
        let data = self.listData!["items"].arrayValue[indexPath.row];
        
        let unit = self.listData!["unit"].stringValue
        let value = data["value"].stringValue;
        
        let formatter = NSNumberFormatter();
        formatter.numberStyle = NSNumberFormatterStyle.DecimalStyle;
        
        let doubleFormatter = NSNumberFormatter();
        doubleFormatter.maximumFractionDigits = 2;
        let intVal = Int(value);
        
        let doubleVal = Double(value);
        let formatted: String = intVal != nil ? formatter.stringFromNumber(NSNumber(integer: intVal!))! : doubleFormatter.stringFromNumber(NSNumber(double: doubleVal!))!;
        
        
        
        
        
        transform.unitLabel.text = formatted+" \(unit)";
        
        transform.titleLabel.text = data["name"].stringValue;
        
        transform.extraLabel.text = data["extra"].stringValue;
        transform.rankNumber.text = String(indexPath.row + 1)+"";
     //   transform.rankNumber.backgroundColor = self.backgroundColor
        transform.contentView.backgroundColor = UIColor.clearColor()
        
        let thumbnail = data["data"]["thumbnail"].stringValue
        let url = NSURL(string: "http://top10.asccio.net/images/\(thumbnail)");
        
        transform.previewImage.layer.cornerRadius = 1;
        transform.previewImage.layer.masksToBounds = true;
        transform.previewImage.kf_setImageWithURL(url!, placeholderImage: nil);
   
        if(indexPath.row % 2 != 0) {
//            transform.backgroundColor = UIColor.init(hex: "f2f2f2")
        }

        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        let destination = segue.destinationViewController as! SwipeListControllerViewController;
        
        destination.items = self.listData!["items"].arrayValue;

        destination.categoryData = self.listData!;
    }

}
