//
//  SwipeListControllerViewController.swift
//  Top10New
//
//  Created by daniel hadland on 17/07/2016.
//  Copyright © 2016 daniel hadland. All rights reserved.
//

import UIKit
import SwiftyJSON

class SwipeListControllerViewController: UIPageViewController, UIPageViewControllerDataSource {

    
    public var items: [JSON]?;
    
    public var categoryData: JSON?;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataSource = self
        self.setViewControllers([getViewControllerAtIndex(0)] as [UIViewController], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil);
        
        
        let image = UIImageView(image: UIImage(named: "top10logo"));
        
        image.frame = CGRect(x: 0, y: 0, width: 88, height: 44);
        image.contentMode = .ScaleAspectFit
        self.navigationItem.titleView = image;
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController?
    {

        let pageContent: ViewRankController = viewController as! ViewRankController
        var index = pageContent.pageIndex
        if (index == NSNotFound)
        {
            return nil;
        }
        index += 1;
        if (index == self.items!.count)
        {
            return nil;
        }
    
        return getViewControllerAtIndex(index);

    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController?
    {
        
        let pageContent: ViewRankController = viewController as! ViewRankController
        var index = pageContent.pageIndex
        if ((index == 0) || (index == NSNotFound))
        {
            return nil
        }
        index -= 1;
  
        return getViewControllerAtIndex(index);
    }
    
    func getViewControllerAtIndex(index: NSInteger) -> UIViewController {
        // Create a new view controller and pass suitable data.
        let pageContentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ViewRankList") as! ViewRankController
        
        let data = self.items![index].dictionaryValue;

        
        pageContentViewController.pageIndex = index;
        pageContentViewController.data = data;
        pageContentViewController.listTitle = self.categoryData?.dictionaryValue["name"]?.stringValue;
        
        
         pageContentViewController.unitValue = self.categoryData?.dictionaryValue["unit"]?.stringValue;
        
    
        return pageContentViewController
        
    }
    
}
