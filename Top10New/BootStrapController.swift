//
//  BootStrapController.swift
//  Top10New
//
//  Created by daniel hadland on 03/07/2016.
//  Copyright © 2016 daniel hadland. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class BootStrapController: UIViewController {

    let loadURL: String = "http://top10.asccio.net/cats"
    let searchTableURL: String = "http://top10.asccio.net/search";

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewDidAppear(animated: Bool) {
        self.load()
        
    }
    
    func loadFail()
    {
        let alertController = UIAlertController(title: "Network Error", message: "Top10 needs to connect to the internet to download the latest data, please check your connection and try again.", preferredStyle: .Alert)
        

        
        let OKAction = UIAlertAction(title: "Retry", style: .Default) { (action) in
            self.load()
        }
        
        alertController.addAction(OKAction)
        
        let destroyAction = UIAlertAction(title: "Quit Application", style: .Destructive) { (action) in
            exit(0);
        }
        
        alertController.addAction(destroyAction)
        
        self.presentViewController(alertController, animated: true) {
        }
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
    }
    
    
    func load() {
        Alamofire.request(.GET, self.loadURL, encoding:.JSON).responseJSON
            { response in switch response.result {
                
            case .Success(let data):
                
                let response = JSON(data);
                
                App.config = response;
                
                self.performSegueWithIdentifier("movenav", sender: self);
                
                break;
            case .Failure(let error):
                print("error \(error)");
                self.loadFail()
                break;
                
                }
        }
        
        Alamofire.request(.GET, self.searchTableURL, encoding:.JSON).responseJSON
            { response in switch response.result {
                
            case .Success(let data):
                
                let response = JSON(data);
                
                App.searchTable = response;
                
                
                break;
            case .Failure(let error):
                print("error loading search \(error)");
                break;
                
                }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
