//
//  NavCell.swift
//  Top10New
//
//  Created by daniel hadland on 21/07/2016.
//  Copyright © 2016 daniel hadland. All rights reserved.
//

import UIKit

class NavCell: UITableViewCell {

    @IBOutlet weak var categoryLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
