//
//  SearchCell.swift
//  Top10New
//
//  Created by daniel hadland on 02/08/2016.
//  Copyright © 2016 daniel hadland. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {
    @IBOutlet weak var imageOutlet: UIImageView!

    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
