//
//  ViewCategoryController.swift
//  Top10New
//
//  Created by daniel hadland on 03/07/2016.
//  Copyright © 2016 daniel hadland. All rights reserved.
//

import UIKit
import SwiftyJSON


private let reuseIdentifier = "Ce2"

class ViewCategoryController: UICollectionViewController {

    var categoryColour: UIColor?
    var categoryData: [String: JSON]?;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return getSections().count
    }
    
    func getSections() -> [JSON] {
        
        let sections =  self.categoryData!["sections"];
        return sections!.arrayValue;
    }
    
    
    func getSectionList(key: Int) -> [JSON] {
        let sections = self.getSections();
        return sections[key]["lists"].arrayValue;
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           let count =  getSectionList(section).count
        return count;
    }
    

    override func collectionView(collectionView: UICollectionView,
                                 viewForSupplementaryElementOfKind kind: String,
                                                                   atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        //1
        switch kind {
        //2
        case UICollectionElementKindSectionHeader:
            //3
            let headerView =
                collectionView.dequeueReusableSupplementaryViewOfKind(kind,
                                                                      withReuseIdentifier: "section",
                                                                      forIndexPath: indexPath)            
            return headerView
        default:
            //4
            assert(false, "Unexpected element kind")
        }
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("list", forIndexPath: indexPath)
        
        // Configure the cell
        
    
        cell.backgroundColor = self.categoryColour;
        return cell
    }

   

}
