//
//  TableViewController.swift
//  Top10New
//
//  Created by daniel hadland on 03/07/2016.
//  Copyright © 2016 daniel hadland. All rights reserved.
//

import UIKit

import SwiftyJSON
import Kingfisher

extension UIColor{
    func adjust(red: CGFloat, green: CGFloat, blue: CGFloat, alpha:CGFloat) -> UIColor{
        var r: CGFloat = 0, g: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
        var w: CGFloat = 0
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        return UIColor(red: r+red, green: g+green, blue: b+blue, alpha: a+alpha)
    }
}


class TableViewController: UITableViewController {


    
    var categoryColour: UIColor?
    
    
    var categoryData: [String: JSON]?;
    let gradientLayer = CAGradientLayer()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let color = UIColor(hue: 0.2556, saturation: 0, brightness: 0.95, alpha: 1.0)
        
        let toColor = UIColor.whiteColor()
        
        self.setTableViewBackgroundGradient(self, color, toColor);
        
        let image = UIImageView(image: UIImage(named: "top10logo"));
        
        image.frame = CGRect(x: 0, y: 0, width: 88, height: 44);
        image.contentMode = .ScaleAspectFit
        self.navigationItem.titleView = image;
        
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if(section == 0){
            let cell = tableView.dequeueReusableCellWithIdentifier("bgcell") as! HeaderCell;
            cell.mainTitle.text = self.categoryData!["main_title"]?.stringValue;
            cell.subTitle.text = self.categoryData!["subtitle"]?.stringValue;
            
            let id = self.categoryData!["id"];
            let urlpath = "http://top10.asccio.net/banners/\(id!).png";
            
            print(urlpath);
            
            cell.backgroundImage.kf_setImageWithURL(NSURL(string: urlpath)!);
            return cell;
        } else {
        let cell = tableView.dequeueReusableCellWithIdentifier("section");
        
        cell!.backgroundColor = self.categoryColour
                navigationController?.navigationBar.backgroundColor = self.categoryColour
                navigationController?.navigationBar.tintColor = self.categoryColour
        return cell;
        }
        
    }
    
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
     
        return self.getSections().count + 1
    }

    
    
   override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
    
        if(section == 0) {
            return;
        }
    
        let sections = self.getSections();
        
        let title = sections[section - 1]["name"].stringValue;
        let cell = view as! SectionHeader
        
        cell.titleLabel.text = title;
        let thumbnail = sections[section - 1]["thumbnail"].stringValue;
    
        let urlpath = "http://top10.asccio.net/sections/\(thumbnail)";
    
        cell.icon.kf_setImageWithURL(NSURL(string: urlpath)!);
    
    }

    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 200;
        } else {
            return tableView.sectionHeaderHeight;
        }
    }
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sections = self.getSections();
        if(section == 0) {
            return "n/a";
        }
        
        return sections[section - 1]["name"].stringValue;
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if(section == 0) {
            return 0;
        }
        return getSectionList(section - 1).count
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {

    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
                
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        
        let item = self.getSectionList(indexPath.section - 1)[indexPath.row];
        let top10cell = cell as! Top10ListCell
        
        
        
        top10cell.listTitle.text = item.dictionaryValue["name"]?.stringValue.capitalizedString
        
        let data = item.dictionaryValue["id"]!.stringValue;
        
        let key = "read_\(data)";
        let defaults = NSUserDefaults.standardUserDefaults();
        
        let read = defaults.boolForKey(key);
        
        print("read \(key) to \(read) lol");
        if(read == true) {
           top10cell.listTitle.textColor = UIColor.lightGrayColor();
        } else {
            top10cell.listTitle.textColor = UIColor.blackColor();
        }
        
        
        let thumbnail = item.dictionaryValue["thumbnail"]?.stringValue
        let url = NSURL(string: "http://top10.asccio.net/images/\(thumbnail!)");
        
        top10cell.listThumbnail.kf_setImageWithURL(url!, placeholderImage: nil);
        top10cell.backgroundColor = self.categoryColour?.adjust(-0.2, green: -0.2, blue: -0.2, alpha: 1.0)
        self.tableView.backgroundView?.backgroundColor = top10cell.backgroundColor

        top10cell.listThumbnail.layer.masksToBounds = true;
        top10cell.listData = self.getSectionList(indexPath.section - 1)[indexPath.row];
        top10cell.backgroundColor = UIColor.clearColor()
        return cell
    }
    
    
    func setTableViewBackgroundGradient(sender: UITableViewController, _ topColor:UIColor, _ bottomColor:UIColor) {
    
    let gradientBackgroundColors = [topColor.CGColor, bottomColor.CGColor]
    let gradientLocations = [0.0,1.0]
    
    let gradientLayer = CAGradientLayer()
    gradientLayer.colors = gradientBackgroundColors
    gradientLayer.locations = gradientLocations
    
    gradientLayer.frame = sender.tableView.bounds
    let backgroundView = UIView(frame: sender.tableView.bounds)
    backgroundView.layer.insertSublayer(gradientLayer, atIndex: 0)
    sender.tableView.backgroundView = backgroundView
    }
    
    
    func getSections() -> [JSON] {
        
        let sections =  self.categoryData!["sections"];
        return sections!.arrayValue;
    }
    
    
    func getSectionList(key: Int) -> [JSON] {
        let sections = self.getSections();
        return sections[key]["lists"].arrayValue;
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
            let target = segue.destinationViewController as! ViewTop10ListController
            target.backgroundColor = self.categoryColour?.adjust(-0.25, green: -0.25, blue: -0.25, alpha: 1)
            target.navigationController?.navigationBar.backgroundColor = self.categoryColour
            let cell = sender as! Top10ListCell
        target.navigationController?.title = cell.listTitle.text
        target.title = cell.listTitle.text
        target.backgroundColor = self.categoryColour
        target.listData = cell.listData;
        
        let defaults = NSUserDefaults.standardUserDefaults();
        let data = cell.listData!["id"];
        cell.listTitle.textColor = UIColor.lightGrayColor();
        
        let key = "read_\(data)";
        
        let read = defaults.boolForKey(key);
        let category_id = self.categoryData!["id"];
        
        if(!read){
            let cat_key = "cat_count_\(category_id)";
            var totalRead = defaults.integerForKey(cat_key);
            totalRead += 1;
            defaults.setInteger(totalRead, forKey: cat_key);
            CategoryController.singleton!.collectionView!.reloadData();
        }
        defaults.setBool(true, forKey: key);
        
        
    }
}
