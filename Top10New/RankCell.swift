//
//  RankCell.swift
//  Top10New
//
//  Created by daniel hadland on 05/07/2016.
//  Copyright © 2016 daniel hadland. All rights reserved.
//

import UIKit

class RankCell: UITableViewCell {
    @IBOutlet weak var layerBg: UIView!

    @IBOutlet weak var backgroundViewColor: UILabel!
    @IBOutlet weak var backgroundView2: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let width = UIScreen.mainScreen().bounds.size.width
        
        print("screen width: \(width)");
        
       
    }
    
    

    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var labelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var previewImage: UIImageView!
    @IBOutlet weak var extraLabel: UILabel!
    @IBOutlet weak var rankNumber: UILabel!
    @IBOutlet weak var unitLabel: UILabel!
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
