//
//  SectionHeader.swift
//  Top10New
//
//  Created by daniel hadland on 07/07/2016.
//  Copyright © 2016 daniel hadland. All rights reserved.
//

import UIKit

class SectionHeader: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var icon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
      
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
