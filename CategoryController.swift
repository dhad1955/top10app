//
//  CategoryController.swift
//  Top10New
//
//  Created by daniel hadland on 03/07/2016.
//  Copyright © 2016 daniel hadland. All rights reserved.
//

import UIKit
import SwiftyJSON
import Spring
import Kingfisher

private let reuseIdentifier = "Cell"


class CategoryController: UICollectionViewController {
    
    @IBOutlet weak var progressView: UIView!
    
    static var singleton: CategoryController? = nil;
    
    // OUTLETS
    
    @IBOutlet weak var leftImageConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightImageConstraint: NSLayoutConstraint!
    @IBOutlet weak var openList: UIBarButtonItem!
    
    // CLASS VARS
    
    var categories : [JSON]?
    var itemSize: CGFloat = 0;
    static var catsGlobal : [JSON]?;
    

    override func viewDidLoad() {
        
        
        let appDomain = NSBundle.mainBundle().bundleIdentifier!
        NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain);
        
        openList.target = self.revealViewController();
        openList.action = Selector("revealToggle:");
        
        super.viewDidLoad()
        
        let backgroundImage = UIImageView(frame: UIScreen.mainScreen().bounds)
        backgroundImage.image = UIImage(named: "app-background")
        backgroundImage.contentMode = UIViewContentMode.ScaleAspectFill
        self.view.insertSubview(backgroundImage, atIndex: 0)
        self.view.backgroundColor = UIColor.clearColor();

        categories = App.config!.arrayValue;
        self.collectionView?.reloadData()
        
        
        let layout = self.collectionViewLayout as! UICollectionViewFlowLayout
        
        let calc = (self.collectionView!.frame.width - 30) / 3.0;
        
        layout.itemSize = CGSize(width: calc, height: calc);
        self.itemSize = calc;
        
        let image = UIImageView(image: UIImage(named: "top10logo"));
        
        image.frame = CGRect(x: 0, y: 0, width: 88, height: 44);
        image.contentMode = .ScaleAspectFit
        self.navigationItem.titleView = image;
        CategoryController.singleton = self;
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1; // 1 CATEGORY
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if(segue.identifier == "gosearch") {
            return;
        }
        
        let viewCategoryController = segue.destinationViewController as! TableViewController
        let categoryCell = sender as! CategoryCell
        viewCategoryController.categoryColour = UIColor(hex: "00a0e3")
        
        
        viewCategoryController.navigationController?.title = categoryCell.catName.text
        viewCategoryController.categoryData = categoryCell.category;
        viewCategoryController.title = categoryCell.catName.text
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(self.categories == nil) {
            return 0;
        }
        
        return self.categories!.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath)
        
        let cat = cell as! CategoryCell
        
        
        let category = self.categories![indexPath.row];
        
        cat.catName.text = category["title"].stringValue;
        cat.category = category.dictionaryValue
        
        cat.catName  .numberOfLines = 2;
        
        cat.scale(self.itemSize)
        
        cell.layer.cornerRadius = self.itemSize / 2;
        
        cell.layer.borderColor = UIColor.whiteColor().CGColor;
        cell.layer.borderWidth = 1.0;
        
        cell.layer.masksToBounds = true;
        
        let frame = cat.icon.frame;
        let newFrame = CGRect(x:0, y: 0, width: self.itemSize / 3, height: self.itemSize / 3)
        
        let thumbnail = category["thumb"].stringValue;
        let url =  NSURL(string: "http://top10.asccio.net/categories/\(thumbnail).png")
        cat.icon.kf_setImageWithURL(url!);
        
        let progress =  cat.progressConstraint;
        
        progress.constant = CGFloat(Float(itemSize) * cat.getPercentRead());
    
        print("progress \(cat.getPercentRead())");
            
        return cell
    }
    
    
    
    override func collectionView(collectionView: UICollectionView,
                                 viewForSupplementaryElementOfKind kind: String,
                                                                   atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let headerView =
                collectionView.dequeueReusableSupplementaryViewOfKind(kind,
                                                                      withReuseIdentifier: "headsection",
                                                                      forIndexPath: indexPath)
            return headerView
        default:
            assert(false);
            break;
            
        }
    }
    
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet() as NSCharacterSet).uppercaseString
        
        if (cString.hasPrefix("#")) {
            cString = cString.substringFromIndex(cString.startIndex.advancedBy(1))
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.grayColor()
        }
        
        var rgbValue:UInt32 = 0
        NSScanner(string: cString).scanHexInt(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
